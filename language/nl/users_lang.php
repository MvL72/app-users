<?php

$lang['users_account_locked'] = 'Vergrendeld';
$lang['users_account_lock_status'] = 'Vergrendel status gebruikersaccount';
$lang['users_account_unlocked'] = 'Ontgrendeld';
$lang['users_added_user'] = 'Toegevoede gebruiker';
$lang['users_add_user'] = 'Voeg gebruiker toe';
$lang['users_app_description'] = 'Met de gebruikersapp kan een beheerder gebruikers op het systeem maken, verwijderen en wijzigen. Andere apps die rechtstreeks in de gebruikersdirectory worden ingevoegd, geven automatisch opties weer die beschikbaar zijn voor een gebruikersaccount.';
$lang['users_app_name'] = 'Gebruikers';
$lang['users_app_policies'] = 'App beleid';
$lang['users_apps'] = 'Apps';
$lang['users_create_group'] = 'Groep maken';
$lang['users_deleted_account'] = 'Verwijderd account';
$lang['users_extension'] = 'Extension';
$lang['users_first_name'] = 'Voornaam';
$lang['users_first_name_invalid'] = 'Voornaam is ongeldig.';
$lang['users_full_name_already_exists'] = 'Een gebruiker met dezelfde volledige naam bestaat al.';
$lang['users_full_name'] = 'Voor en achternaam';
$lang['users_gid_number'] = 'Groep ID nummer';
$lang['users_group_id_invalid'] = 'Groep ID is ongeldig.';
$lang['users_groups'] = 'Groepen';
$lang['users_home_directory'] = 'Home directory';
$lang['users_home_directory_invalid'] = 'Home directory is ongeldig.';
$lang['users_last_name_invalid'] = 'Achternaam is ongeldig.';
$lang['users_last_name'] = 'Achternaam';
$lang['users_lock'] = 'Vergrendeld';
$lang['users_login_shell_invalid'] = 'Login shell is ongeldig.';
$lang['users_login_shell'] = 'Login shell';
$lang['users_name'] = 'Naam';
$lang['users_old_password_invalid'] = 'Oude wachtwoord is ongeldig.';
$lang['users_options'] = 'Opties';
$lang['users_password_cannot_be_changed_on_this_account'] = 'Wachtwoord van dit account kan niet worden gewijzigd.';
$lang['users_password_in_history'] = 'Wachtwoord komt voor in de geschiedenis van de oude wachtwoorden.';
$lang['users_password_invalid'] = 'Wachtwoord is ongeldig.';
$lang['users_password_is_too_short'] = 'Wachtwoord is te kort.';
$lang['users_password_is_too_young'] = 'Wachtwoord is te jong om te veranderen.';
$lang['users_password_not_changed'] = 'Wachtwoord wordt niet gewijzigd.';
$lang['users_password_violates_quality_check'] = 'Wachtwoord faalt kwaliteitscontrolebeleid.';
$lang['users_password_was_reset'] = 'Wachtwoord vereist een reset.';
$lang['users_plugins'] = 'Plugins';
$lang['users_reset_password_on_account'] = 'Reset wachtwoord';
$lang['users_uid_number'] = 'Gebruikers ID Nummer';
$lang['users_unlock'] = 'Ontgrendelen';
$lang['users_updated_settings_on_account'] = 'Accountinstellingen bijgewerkt';
$lang['users_user_already_exists'] = 'Gebruikersnaam bestaat al.';
$lang['users_user_details'] = 'gebruikersdetails';
$lang['users_user_id_invalid'] = 'Gebruikers ID is ongeldig.';
$lang['users_user_information_invalid'] = 'Informatie voor de gebruiker is ongeldig.';
$lang['users_user_manager'] = 'Gebruikersmanager';
$lang['users_username_invalid'] = 'Gebruikersnaam is ongeldig.';
$lang['users_username_is_reserved'] = 'Gebruikersnaam is gereserveerd.';
$lang['users_user_not_found'] = 'Gebruiker niet gevonden.';
$lang['users_users_and_groups'] = 'Gebruikers en groepen';
$lang['users_users_storage'] = 'Gebruikers opslag';
$lang['users_users'] = 'Gebruikers';
$lang['users_user'] = 'Gebruiker';
